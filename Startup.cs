﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Bad_Address_Project_112116.Startup))]
namespace Bad_Address_Project_112116
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
