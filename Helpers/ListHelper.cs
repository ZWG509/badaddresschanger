﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Bad_Address_Project_112116.Helpers
{
    public class ListHelper
    {

        public static SelectList GetDecisionSelect(Models.Decision decision)
        {
            Dictionary<int, string> Dic = new Dictionary<int, string>();
            foreach (Bad_Address_Project_112116.Models.Decision Dec in Enum.GetValues(typeof(Bad_Address_Project_112116.Models.Decision)))
            {
                Dic.Add((int)Dec, Dec.ToString());
            }

            return new SelectList(Dic, "Key", "Value", (int)decision);
        }
    }

    
}