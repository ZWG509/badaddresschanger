﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bad_Address_Project_112116.Testing;

namespace Bad_Address_Project_112116.Models
{

    public enum Decision
    {
        DoNothing, 
        ChangeAddress,
        //ChangeMr,
        //ChangeAddressAndMr

    }

    public class AddressChangeItem
    {
        public AddressChangeAccount GoodAccount { get; set; }
        public AddressChangeAccount BadAccount { get; set; }
        

        public Decision RecommendedDecision { get; set; }

        [System.ComponentModel.DataAnnotations.Required]
        public Decision OverriddenDecision { get; set; }

        public string OverrideDecisionText { get; set; }

        public AddressChangeResult Result { get; set; }

        internal void Load(LatitudeAddressEvalReportItem d)
        {
            GoodAccount = new AddressChangeAccount();
            GoodAccount.LoadGood(d);

            BadAccount = new AddressChangeAccount();
            BadAccount.LoadBad(d);

            GetRecommendedDecision();

        }       

        public void GetRecommendedDecision()
        {
            RecommendedDecision = Decision.ChangeAddress;
            OverriddenDecision = RecommendedDecision;
        }
    }


    public class AddressChangeModel
    {
        public List<AddressChangeItem> Items { get; set; }
        public int ResultID { get; set; }
        public bool Valid { get; set; }
        public bool DisplayErrorMessage { get { return !Valid; } }

        internal bool CustomValidate()
        {
            bool flag = true;
            foreach (var item in Items)
            {
                if (item.RecommendedDecision != item.OverriddenDecision && string.IsNullOrEmpty(item.OverrideDecisionText))
                {
                    flag = false;
                    item.Result = new AddressChangeResult()
                    {
                        Success = false,
                        Message = "No overridden decision given."
                    };
                }
                else
                {
                    item.Result = new AddressChangeResult();
                }
            }
            Valid = flag;
            return flag;
        }


        internal void Execute()
        {
            ResultID = ResultID;
        }

        internal void Load()
        {
            Valid = true;

            List<Testing.LatitudeAddressEvalReportItem> Data = Testing.LatitudeAddressEvalReportItem.GetTestData();
            Items = new List<AddressChangeItem>();

            foreach (var d in Data)
            {
                AddressChangeItem item = new AddressChangeItem();
                item.Load(d);
                Items.Add(item);

            }

        }

    }

    public class AddressChangeResult
    {
        public string Message { get; internal set; }
        public bool Success { get; set; }
    }

    public class AddressChangeAccount
    {
        public Address Address { get; set; }
        public int FileNumber { get; set; }
        public string MailReturn { get; set; }

        internal void LoadBad(LatitudeAddressEvalReportItem d)
        {
            Address = new Address();
            Address.LoadBad(d);
            FileNumber = Convert.ToInt32(d.BadNumber);
            MailReturn = d.BadMailReturn;
        }

        internal void LoadGood(LatitudeAddressEvalReportItem d)
        {
            Address = new Models.Address();
            Address.LoadGood(d);
            FileNumber = Convert.ToInt32(d.GoodNumber);
            MailReturn = d.GoodMailReturn;
        }
    }
    
    public class Address
    {
        public string StreetOne { get; set; }
        public string StreetTwo { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }

        internal void LoadBad(LatitudeAddressEvalReportItem d)
        {
            StreetOne = d.BadStreet1;
            StreetTwo = d.BadStreet2;
            City = d.BadCity;
            State = d.BadState;
            ZipCode = d.BadZipCode;
        }

        internal void LoadGood(LatitudeAddressEvalReportItem d)
        {
            StreetOne = d.GoodStreet1;
            StreetTwo = d.GoodStreet2;
            City = d.GoodCity;
            State = d.GoodState;
            ZipCode = d.GoodZipCode;
        }
    }

   

   



}







