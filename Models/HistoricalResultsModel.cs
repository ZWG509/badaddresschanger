﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bad_Address_Project_112116.Models
{
    public class HistoricalResultsModel
    {
        public int ResultID { get; set; }
        public bool ResultsFound { get { return ResultID > 0; } }

        internal void Load(int resultID)
        {
            ResultID = resultID;

        }
    }
}