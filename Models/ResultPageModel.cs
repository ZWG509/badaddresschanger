﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bad_Address_Project_112116.Models
{
    public class ResultPageModel
    {
        public LinksModel Links { get; set; }
        public ResultPanelModel Results { get; set; }

    }

    public class LinksModel
    {
       public List<ResultLink> Links { get; set; }

    }
       public class ResultLink
    {

        public string Action { get; set; }
        public string Controller { get; set; }
        public string DisplayText { get; set; }
        public int ResultID { get; set; }
    }

    public class ResultPanelModel
    {
        public List<ResultItem> ResultItems { get; set; }
        public DateTime Date { get; set; }
        public string UserName { get; set; }
        public int Overridden { get; set; }
        public int Changed { get; set; }
        public bool AnyResultsFound { get; set; }
        public int ResultID { get; set; }
    }

    public class ResultItem
    {

        public int FileNumber { get; set; }
        public string OverrideReason { get; set;}
        public Decision Decision { get; set; }
        public bool IsOverridden { get; set; }
    }


} 