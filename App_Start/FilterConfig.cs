﻿using System.Web;
using System.Web.Mvc;

namespace Bad_Address_Project_112116
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
