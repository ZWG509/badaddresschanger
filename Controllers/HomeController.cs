﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Bad_Address_Project_112116.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {

            Models.AddressChangeModel model = new Models.AddressChangeModel();
            model.Load();

            return View(model);
        }

        [HttpPost]
        public ActionResult RunAddressChange(Models.AddressChangeModel model)
        {
            bool valid = model.CustomValidate();

            if (!ModelState.IsValid || !valid)
            {

                return View("Index", model);
            }

            model.Execute();

            return RedirectToAction("Results", new { @ResultID = model.ResultID });
            //branching if...show contact page. o
        }

        public ActionResult Results(int ResultID = 0)
        {
            Models.HistoricalResultsModel model = new Models.HistoricalResultsModel();
            model.Load(ResultID);
            return View(model);
        }



        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }


        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult History()
        {
            Models.GetHistoryRequestModel model = new Models.GetHistoryRequestModel();

            return View(model);
        }

        [HttpPost]
        public ActionResult GetHistory(Models.GetHistoryRequestModel model)
        {

            return RedirectToAction("Results", new { @ResultID = model.ResultID });
        }

        //public ActionResult ResultsPage(Models.ResultPageModel model)
        //{
        //    //return RedirectToAction
        //}

    } 
}