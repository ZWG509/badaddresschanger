﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bad_Address_Project_112116.Testing
{
    public class LatitudeAddressEvalReportItem
    {
        public string GoodNumber { get; set; }
        public string GoodStreet1 { get; set; }
        public string GoodStreet2 { get; set; }
        public string GoodCity { get; set; }
        public string GoodZipCode { get; set; }
        public string BadNumber { get; set; }
        public string BadStreet1 { get; set; }
        public string BadStreet2 { get; set; }
        public string BadCity { get; set; }
        public string BadZipCode { get; set; }
        public string BadState { get; set; }
        public string GoodState { get; set; }
        public string BadMailReturn { get; set; }
        public string GoodMailReturn { get; set; }

        public static List<LatitudeAddressEvalReportItem> GetTestData()//populate with accounts 5 or so.
        {
            List<LatitudeAddressEvalReportItem> Items = new List<LatitudeAddressEvalReportItem>();

            for (int i = 0; i < 5; i++)
            {
                Items.Add(new LatitudeAddressEvalReportItem()
                {
                    BadCity = "West Chester",
                    BadState = "Pa",
                    BadNumber = (i * 20).ToString(),
                    BadStreet1 = "Lacey",
                    BadZipCode = "19382",
                    BadMailReturn = "Y",
                    GoodCity = "West Chester",
                    GoodState = "Pa",
                    GoodNumber = i.ToString(),
                    GoodStreet1 = "123 Street Ave",
                    GoodZipCode = "19232",
                    GoodMailReturn = "N"
                    
                });

            }

            return Items;
                   
        }
        
    }

   
}